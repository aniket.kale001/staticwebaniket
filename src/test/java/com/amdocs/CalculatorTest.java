import com.amdocs.Calculator;
import org.junit.Test;
import org.junit.Assert;

public class CalculatorTest
{
	@Test
	public void add()
	{
		Calculator c = new Calculator();
		int result1 = c.add();
		Assert.assertEquals(9,result1);
	}

	@Test
        public void sub()
        {
		Calculator c = new Calculator();
                int result2 = c.sub();
                Assert.assertEquals(3,result2);
        }
}
